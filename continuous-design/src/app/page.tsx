import Link from "next/link";

export default function Home() {
  return (
    <main className="grid gap-4 grid-section pt-12">
      <div></div>
      <div className="prose max-w-none prose-invert">
        <h1>
          Continuous Design
          <div className="text-lg italic text-slate-500 pl-24">
            Small takeaways about Code, Design and Software Architecture
          </div>
        </h1>
        <div className="text-right">
          <Link href="about">About</Link>
        </div>
        <hr className="m-6" />

        <ul>
          <li>
            <Link href="tidy-first">
              Reading notes on <span className="italic">Tidy First?</span> by
              Kent Beck
            </Link>
          </li>
          <li>
            <Link href="modern-software-engineering">
              Reading notes on{" "}
              <span className="italic">Modern Software Engineering</span> by
              David Farley
            </Link>
          </li>
          <li>
            <Link href="accelerate">
              Reading notes on <span className="italic">Accelerate</span> by
              Nicole Forsgren, Jez Humble and Gene Kim
            </Link>
          </li>
        </ul>
      </div>
      <div></div>
    </main>
  );
}
