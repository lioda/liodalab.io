import { PageLayout } from "../../components/page-layout";

<PageLayout date="2024-03-28">

# Reading notes: _Accelerate - Building and Scaling High Performing Technology Organizations_

_Accelerate - Building and Scaling High Performing Technology Organizations_, is the condensate analysis of a huge survey across our industry.

What are the great performers in Software Engineering, and overall, what do they do to perform well?

![cover](/accelerate/cover.png)

In the book, authors are not only sharing their discoveries, but also the whole method and data used to reach their conclusions.

There are 24 key capabilities classified in 5 categories: DevOps and Continuous Delivery, Software Architecture and Design, Product and Process Capabilities, Lean Management and Culture Shift.

We’ll walk through these points.

# Accelerate

Nowadays, to remain competitive each business needs to accelerate its delivery of goods and services, analyze of customers needs and market trends, respond to risks and regulatory changes.

> At the heart of this acceleration is software.

## Capabilities

Instead of trying to apply a change as a model, organizations should focus on capabilities to drive the change:

- to follow a continuous improvement paradigm
- they allow adaptation and customization
- to put improvement and outcomes at heart of the methodology
- it highlights the need to develop skills to remain competitive

## Measuring performances

The book authors emphasize that to be efficient any methodology must be measured. To define what is good in a context, organizations need indicator at a global level (to avoid pathological competition between teams) and to be focused on outcome, not output.

They settle four measures:

- **Delivery Lead Time**
  - the time a request take to go from customer to satisfaction
  - time to design and validate a product feature
  - time to deliver a feature to customers
- **Deployment Frequency**
  - deployment to production or an app store
  - this indicator is also a proxy for batch size and efficiency
- Mean Time To Restore (MTTR)
  - failure is inevitable, the question is: _How quickly can service be restored?_
  - reliability is measured as time between failures
- Change Fail Rate
  - what percentage of change (at any level: software, infrastructure…) leads to failure
  - A key in quality metric

## Measuring Culture

A good organizational culture has an impact on performance, both organizational and for deliveries.

Sociologist Ron Westrum described 3 culture models:

- **Pathological** (power-oriented) organizations are characterized by large amounts of fear and threat. People often hoard information or withhold it for political reasons, or distort it to make themselves look better.
- **Bureaucratic** (rule-oriented) organizations protect departments. Those in the department want to maintain their “turf,” insist on their own rules, and generally do things by the book—their book.
- **Generative** (performance-oriented) organizations focus on the mission. How do we accomplish our goal? Everything is subordinated to good performance, to doing what we are supposed to do.
  - Better collaboration
  - Mission is primary
  - Less hierarchy

They are related to information management and how it is emitted, hence how it can be used by the receiver or how to avoid errors with better information flow.

To accelerate, organizations need both resilience and the ability to innovate and adapt.
Another study of Google concludes:

> Who is on a team matters less than how the team members interact, structure their work, and view their contributions.

[Source](https://www.thinkwithgoogle.com/intl/en-emea/consumer-insights/consumer-trends/five-dynamics-effective-team/)

## Sustainability

Quality is a mean to reduce pain associated to failures and complexity of deployment.
Each process should be automated and some production-like environment should be available.

Acceleration must be sustainable. Authors discuss about burnout and how to avoid it through a supportive and respectful organizational culture, leader effectiveness and giving employees time and resource to improve their work, allowing them to experiment.

# Capabilities to Drive Improvement

There are 24 capabilities to improve processes, performances and culture in organizations.

## Continuous Delivery

1. **Version Control:** used to store all production artifacts including application code, application configurations, system configurations, and scripts for automating build and configuration of the environment. Version control are enablers to Continuous Delivery.
   This point is rarely debated
2. **Deployment Automation:** no manual intervention required to deploy. Reduce deployment pain, avoid repetitive tasks done by people to enable them working on more interesting problem-solving tasks.
3. **Continuous Integration**: regularly integrate all the code and run a build process including tests to find any regression that need to be immediately fixed. Only the code passing all tests is releasable.
4. **Trunk Based Development:** considered as a predictor of high performance. Fewer than 3 active branches with very short lifetime or less than a day, and no “code freeze” periods.
5. **Test Automation:** no manual intervention to run the tests, nor to interpret the result (i.e. success and failures are part of the test definition, and computers can determine their status).
   It means the test suite must be reliable and software engineers should be responsible for creating and maintaining them
6. **Test Data Management:** have sufficient amount of data to run automated test suites. It also mean to be able to acquire curated data.
7. **Shift Left on Security:** integrate security practices in all stages of development process, like design and testing phases.
8. **Continuous Delivery:** to achieve CD, the software must be kept in a deployable state at any moment. It has an impact on organization, tools, processes and team interactions. It must rely on automated process and easy to reach feedback to know if the system isn’t deployable to fix it quickly.  
   Continuous Delivery has both a huge impact on how teams work and technical outcome like systems architecture and quality.

## Architecture

9. **Loosely Coupled Architecture:** each team should work independently and be able to deploy their applications on demand, without any permissions or complex coordination of other teams.  
   Also, tests shouldn’t need full integrated environment to run.  
   The study shows a correlation between ratio of number of deployment per developer per day and efficiency.
10. **Empowered Teams:** team can choose their tools and technologies to achieve their tasks. Engineers should be able to experiment tools and new paradigms to be effective.

## Product and Process

11. **Gather and Implement Customer Feedback:** organizations needs to actively and regularly seek their customer feedback to improve their products. This feedback may have many form: satisfaction metrics, or customer insights and use some techniques like A/B testing.
12. **Make Flow of Work Visible:** everyone in the teams should be able to know the flow of work, from business to customers. Each product and feature in development should be in an understandable status.
13. **Small Batches:** work should be decomposed in small batches. Objective is to achieve rapid development, fast feedback loops and short lead times. Avoid complex features that requires long-lived feature branches.
14. **Team Experimentation:** developers should be able to try new ideas and update their requirements without any approval from outside their teams. When used to incorpore customer feedback and delivering in quick and small batches, It fosters innovation.

## Lean Management and Monitoring

15. **Lightweight Change Approval:** a fluid process for production changes has a direct positive impact on software delivery performance. It means avoiding some external approval and fostering peer review based processes.
16. **Feedback from Production:** data about running production applications and infrastructure must be analyzed and transformed into exploitable metrics. This metrics are used to make business decisions and align operational goals.
17. **Proactive Notification:** Teams should be alerted in real time of any defects from logging and monitoring systems, thresholds (CPU, memory) or rate-of-change warnings (e.g. CPU usage has increased over the last 10 minutes) or, at least, customers reports.
18. **Limit Work in Progress (WIP):** a clear limit of work-in-progress are used to ensure teams have a sustainable pace and are not overwhelmed, and make constraints explicit. Seeing obstacles drives process improvement to increases throughput and performance, and eventually culture.
19. **Visual Management:** engineers and leadership should be able to access any time to visual information about quality, productivity and current work status (including defects).

### Culture & Leadership

20. **Generative Culture (as outlined by Westrum):** as described above, it describes a healthy organization including a good information flow, cooperation and trust.
21. **Encourage Learning:** learning should be considered as a fundamental and essential investment by organizations
22. **Collaboration among Teams:** breaking the silos to foster interaction and collaboration in all topics: development, operations and security.
23. **Meaningful Work:** employees’ job satisfaction is important to increase their loyalty. Employees should think their work is challenging and meaningful, and consider that their organization values are echoing their own values. They have to exercise their skills and have the tools and resources needed to achieve their job correctly.
24. **Transformational Leadership:** leadership amplifies all technical and process work through five factors:
    - **Vision:** a clear understanding of where the organization is going
    - **Intellectual Stimulation:** challenge must be a source of creativity
    - **Inspirational Communication:** even in incertain moment and complex contexts, communication must be inspiring and motivating
    - **Supportive Leadership:** care about employees and consider their personal needs
    - **Personal Recognition:** each one should obtain compliments for outstanding work or improvement in work quality

![diagram](/accelerate/diagram.jpeg)

# Conclusion

_Accelerate_ contains a lot more than what I summarized here. Details about good practices and their impact on organizations (of any size). The book also describes the methodology used to gather these data and a lot of examples across the industry.

This book is important because it documents what we, as software engineers, think about what we should apply, often missing data to convince. _Accelerate_, using the management language is a serious help to obtain what we need.

</PageLayout>
