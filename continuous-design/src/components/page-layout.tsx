import Link from "next/link";
import React from "react";

export function PageLayout({
  children,
  date,
  title,
}: {
  children: React.ReactNode;
  title?: string;
  date?: string;
}) {
  return (
    <main className="grid gap-4 grid-section pt-9 prose max-w-none prose-invert mb-12">
      <div></div>
      <div>
        <h1>{title ?? "Continuous Design"}</h1>
        <Link href="/">Explore my other writings</Link>
        {date && <div className="text-right italic text-slate-300">{date}</div>}
        <div className="mt-1 mb-3">
          <hr className="m-0" />
        </div>
        {children}
        <hr />
        <Link href="/">Home</Link>
      </div>
      <div></div>
    </main>
  );
}
