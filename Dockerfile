FROM golang:1.9.2

WORKDIR /usr/local/bin

RUN wget https://github.com/gohugoio/hugo/releases/download/v0.120.4/hugo_0.120.4_Linux-64bit.tar.gz && \
  tar xvfz hugo_0.120.4_Linux-64bit.tar.gz

WORKDIR /blog

COPY . /blog

ENTRYPOINT ["hugo"]
CMD [""]
