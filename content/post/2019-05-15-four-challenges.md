---
title: "The Programmer's 4 Challenges"
date: 2019-05-19
draft: false
image: "images/20190515/cover.jpg"
slug: "4-challenges"
description: "The four challenges a programmer needs to overcome before becoming really productive and professional"
tags: [craftsmanship,quality,business]
---

Again, we have the chance to start something new. New project, new feature, new class, new method...  
No matter what, but it's not an old bug fixing, it's time to be creative.

It's cozy but challenging... And above all, as programmers, we must resist some temptations in order to show ourselves as professional as possible.


# Challenge 1: Resist to start coding in emergency

The nice User Story is ready, now it's time to write some code, isn't it?  


## Why is it bad?

Issues and User Stories describes features, code is only a mean to deliver these features.  
Maybe it's to early for coding.

First we have to understand the domain, the business; in a word: understand what value the feature gives at the stakeholders who want it.  
It may be frustrating, but, remember what [Robert C. Martin says](https://blog.cleancoder.com/uncle-bob/2012/01/11/Flipping-the-Bit.html):

> The only way to go fast, is to go well

Any programmer, event with little experience, knows that:

* at the beginning, the problem is vague
* during execution, the problem reveals itself
* at the end, the problem is eventually understood

Why have we to wait the end to understand ? We should begin at the end...

## What a craftsman would do?

Starting at the end needs to formalize answers to three key questions :

* who will use it? 
* why this user will use it? Final user doesn't care about our sotware, she only wants software solves her problems
* when will it be deployed? Without deployment, the feature is useless... 1% of a deployed feature is always more valuable than 100% of some code sleeping in a not yet merged git branch

And fortunately, there is a way for a programmer to do it: **by writing a test**.

Writing test is a real skill, yet often neglected.  
A good test for starting, is not technical, it must express a business intention, with domain wording.  
It allows to describe the problem in a formal way... Think of it as an executable specification.

# Challenge 2: Data is not a feature

Now we know what to do, let's create the model, think about its performances and build a data-intensive application!

... Really?

## why is it bad?

Every student learns SQL and some techniques for creating database.  
And *it is a truth universally acknowledged* that users want to see data. Try to imagine an online shopping without products retrieve in a powerful database. Or what should be a social network without a graph representation of our [filter bubble](https://en.wikipedia.org/wiki/Filter_bubble)?

After all, our issues describe data to display and save.  

But...  
Data is our worst ennemy.  
Data is expensive, think about its storage, replication, backup, process of upgrade, network consumption (with risks of loss, error, needs of retry, etc.).  
Data alone gives nothing to our stakeholders.

Let's look at this classic schema:
```pre
User <-> UI <-> Business Rules <-> Database
```

Do we exactly know what user needs ? What UI will be ? What are the business rules, and how they'll be implemented in Services and Business Objects?  
We can have some intuition, but without all details, could we know how to design the *lowermost layer* which is the Database?

## What a craftsman would do?

We must incorporate a principle to our engineer mindset: **Data is not a goal, it's a mean to achieve our goals.**

Data is not our business value.  
User don't understand how data is stored. And they don't *want* to.

We store data by *accident*, because it's the only way to retrieve it later.  
We have learned SQL because the whole industry believed it was the greater way to store it. It's time to learn new lessons from the past.  
Our current storage capacities allow us to think some new design like flattened documents, key / value, graph, an so on.

The first thing to do is to write all the Business Rules: it's our application true behavior, and by that way, our application value.  
And the UI, of course, which permits the user to interact with Business Rules.  
We need to try all of them, to shorten feedback loops, adapt and satisfies our stakeholder's needs.

Then and only then, we'll know what is to **persist**. Maybe duplication will be required to improve performances, maybe our different use cases will introduce the necessity for many databases with different abilities (like a SQL and an Elasticsearch to index a subset of data)... Let it emerge to fulfill our needs.

# Challenge 3: Do not take decisions 

Ok, so let's start real code (after all, we have even writtent a test in challenge 1, so even the TDD keeper is happy).

We need a web application with some modish libraries, a communication bus (because a simple shared memory between process is so old school), and so many architectural details.

## Why is it bad?

I think I'm becoming predictable and you are thinking: "he will say again we don't know enough to start, so we'll need to give up another reflex".

Yes. But no.

To show something to our stakeholders, we need to take decisions.  
Without our stakeholders feedback, we can't take any decisions...

To break this loop... what about starting without taking any decisions?

## What a craftsman would do?

In fact, the only structure we are pretty sure of, is the business rules, and they're not about frameworks, web UI, and so on.

However we need to choose all of that.  
Thus, we'll apply a good old pattern: `KISS - Keep It Simple and Stupid`.  
Complexity will come soon and then our future self will thank us to work in a simple codebase.

All that is not our domain behavior, are details. They can change later, we have to close no doors.  
Understand that choices are temporary, due to what we currently know.  
We understand our reasons behind all of them. If, later, a reason were to change, we could exactly adjust our system.

So, the first step is to keep doors wide open and deliver the better code we can with Dependency Injections, Liskov Substitution Principle, Decoupling, and all good practices that help us to isolate our domain from details like UI, database or communication way.

# Challenge 4: Fear of typing code

Now, that we have started to "do real work", we'll write as less code as possible because: `less code, less bugs`.


## Why is it bad?

This time, it's true :  
Less production code, less bugs.

But code is not always production code.  
We should ne fear writing code.  
What we should really be scared of are bugs.


## What a craftsman would do?

To avoid bugs... There is a lot of code to write:

* by overcoming challenge 1, we code executable specifications, tests and deployment
* by overcoming challenge 2, we fake memory repositories to show business value before setting up technical databases
* by overcoming challenge 3, we avoid coupling, which often means more code like interface and data structure to isolates modules 

In a way, quality means more code. Getting fast means more code.  

* TDD is time saving. Automated tests will always be a shorter feedback loop than manual debugging  
* different projects share some data structure... It's not always duplication, maybe they will change at different rate or evolve on separate paths (like with different annotation and meta programming). [CCP](https://en.wikipedia.org/wiki/Package_principles) teach us that it can be valuable to separate them.    
Of course, I'm talking about data structure like contracts or DTOs, not Business Object with duplicated behavior...
* the feature we are working on looks a lot like an old one... No precipitation. Let's code the new feature with simple code; it's more easy to refactor afterwards, when the `Don't Repeat Yourself` emerges by itself.

You understand the idea: **we must write more code to do less work.**  
With TDD, it's usual to write a lot of code to quickly validate a solution, but eventually commit less than when working without TDD.

Writing code must not be scary.  
We are all well trained, typing on keyboard is costless.

Really. What should scare us are: bugs, complex basecode without tests, coupled data structures between distant projects, test written after code that do not reveal a business intention (and with technical adaptations), software system impossible to deploy, frustrated stakeholders, too long feedback loops, structuring decisions taken too early, errors hard to find without apnea deep dive in breakable code...

The craftsman attitude is to overcome these challenges to act like the professionals we pretend to be.
