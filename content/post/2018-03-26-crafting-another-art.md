---
title: "Crafting Hobbies"
date: 2018-03-26
draft: false
image: "images/20180326/cover.jpg"
slug: "crafting-hobbies"
description: "We read books, we code every day, but what is missing to our training ?"
tags: [craftsmanship,art,hobbies]
---

Sometimes, we read (and write) bad code. Sometimes we get stuck with problems that gives headache. How is it possible? We are professionals, we code every working day, but we still need to learn...

Software engineering is not an isolated activity. There are many other professions with same problems. All of them require creativity.

Writing code is all about writing. Reading code is all about reading. Engineering is being inventive, despite constraints.

Improving your skills, of course is done by crafting by yourself, reading design patterns books, and pair reviewing source code. But I also advice you to go to the closest museum or art gallery.

# Art, Science and Engineering

The arbitrary distinction made between these three domains is a recent construct[^1]. Think about expressions like "state of the art" when talking about technologies, or remember the most famous computer science book title: [_The Art of computer programming_](https://en.wikipedia.org/wiki/The_Art_of_Computer_Programming).

[^1]: http://www.paulgraham.com/knuth.html

Art, Science an Engineering are simply three ways for creating culture. Methods are not the same, but they have all one thing in common: they come from human brain and consider human rules.

## Painting or sculpting

When we think about museum, the first thing that come in mind is this kind of thing:

![joconde](/images/20180326/joconde.jpg)

At Renaissance, painters have to create their pigments, they studied optic science to understand perspective, and so more.

Italian Renaissance workshops were like our agile teams, bringing together many artists to produce great works.

Sculpture and painting were based upon technical skills, creativity and physical constraints. Biggest pieces were time boxed commands by rich managers.  
Just like software programming.

## Fashion

Few things seem more away from programming than fashion. Geeks are not renowned for their way of dressing.

But what is fashion about ?

* Some constraints related to hardware and materials
* Modelisation driven by a paradigmatic aesthetic. This year, a woman has to enhance her breast, last year she was defined by her legs. Remind me our technology trends...
* This is team working, your poorly made seam will bother your teammates
* Oh no ! This badly designed piece is debt, and the whole dress is torn

<a title="By https://www.flickr.com/photos/unforth/ (https://www.flickr.com/photos/unforth/1303701436/) [CC BY-SA 2.0 (https://creativecommons.org/licenses/by-sa/2.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AChristian_Dior_Dress.jpg"><img width="256" alt="Christian Dior Dress" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Christian_Dior_Dress.jpg/256px-Christian_Dior_Dress.jpg"/></a>

You see where I am going. I can't still choose my clothes alone, neither can I coordinate colors, but I am the first delighted to see a great fashion designer's exhibition because of all it teaches me about my own job.

# Don't just observe

Entertainment gives you products you can consume _as is_.

At the opposite, art wants you to engage yourself. You have to make an effort to understand the artist, her intention and the world she's creating.

[Land art](https://en.wikipedia.org/wiki/Land_art), for example, immerse watcher by unfolding all around her.

What could be the best way to taste an art than to try it?

## What are your hobbies ?

When I was young, I was playing Role Playing Games. I should still be playing if I had time and a good team.  
(Yes, I'm not young, and I've been a geek for a long time <i class="fa fa-smile-o"></i>)

It was a good preparation for my future life. I had to study rule books, learn formulas, understand worlds, create scenario, read magazine. Parties were like a code review, when it was bad, my friends were without mercy.

Now, creative hobbies are more ordinary, and you can draw, sculpt or even do mosaic easily. You like to cook? Excellent! Learn tastes, improve your recipes and share them.  
Art is everywhere. Do it for yourself or with your kids; do it for pleasure, but do it seriously.

If you gain creativity in one field, it will rebound in other fields. If you understand what is intention, how to choose ideas and organize them, even if it is for designing stained glass, you will learn to code better.


## Writing

As I said in introduction, writing code is in fact... well, _writing_.

Writing is the simplest way to communicate information. In fact, it's the foundation of our culture. Before each movie, song or video game there is written documents. Even science progress through papers.

I'm not just talking about code documentation... Your code is already [your documentation](https://en.wikipedia.org/wiki/Self-documenting_code).

Writing (and coding) makes you:

* think about the intention (what you want to tell)
* select the words to build the model of your narrative (how you want to tell it, which information structures to use)
* organize your ideas (how to lead your reader where you want)

But you need to read:

> If you want to be a writer, you must do two things above all others: read a lot and write a lot.
>
> Stephen King


In programming, this is the same. Without reading other's code, you will not improve your skills. But reading isn't all.

I'm proposing something more creative. You can try to write fictions (short stories, novels, fan fictions), technical article, essays, films critics, role playing games scenarios...

Choose the medium you want (twitter, facebook, blog, etc.) the important thing is to write something which need _creation_. It's easy to spit emotions, hate or love. It will not help you construct coherent thinking.

Code is our professional medium.  
When I read code, I can see author's habit to read/write, according to many clues like [abstraction levels in a method](http://principles-wiki.net/principles:single_level_of_abstraction) or her [naming convention](https://en.wikipedia.org/wiki/Naming_convention_(programming)).  
And when someone expose her problems, she unintentionally exposes another clues, like synthesis capacity or hability to get to the point. Train yourself by writing, where it's more easy to erase mistakes...


# Never get stuck

The team who was working on _Super Mario Bros 3_ was inspired by a journey at Disneyland.

To think about something else, another medium, open new possibilities.

Sherlock Holmes who play violin all night while thinking about mysteries is, beyond the stereotype, an illustration of the detachment sometimes needed to move forward.

The rule is to stay creative and productive.

I can't promise that building your scale model will unfreeze yourself with your scaling microservice written in go.

[![scale model](/images/20180326/scale_model.jpg)  
Source: https://sartenada.wordpress.com/tag/scale-model/](https://sartenada.wordpress.com/tag/scale-model/)

But unleash your creativity and it will be more easy to stay productive...

# Conclusion

Whatever it is: educating your children, taking care of your garden, riding horses, drawing comics, sculpting, modeling, cooking... I don't care, but do it with pleasure and passion. Crafting is a mindset, use it everywhere. Museums are full of inspiration if you know how to read it.

Help your code, craft your hobbies.
