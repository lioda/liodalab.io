---
title: "Go HTTP server testing"
date: 2018-04-10
draft: false
image: "images/20180410/cover.jpg"
slug: "golang-http-testing"
description: "Golang comes with a efficient HTTP Server, but testing isn't always easy"
tags: [craftsmanship,tdd,quality,testing,golang]
---

The Go language comes with a very powerful HTTP server, but we don't only need performances or ease of use. We want at least testability, TDD in the best case.

I'll try to summarize my experience in this little technical post.

# Simple Web server

Look at this elegant tiny code:

```go
package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hello world")
}

func main() {
	http.HandleFunc("/a-path", handler)
	http.ListenAndServe(":8000", nil)
}
```

A web server starts on port `8000` and listen on `/a-path` to return a [greeting message](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program).

That's all... Yeah, that's what I mean by "elegant" <i class="fa fa-smile-o"></i>

# Unit testing

According to [Michael Feathers](https://www.pearson.com/us/higher-education/program/Feathers-Working-Effectively-with-Legacy-Code/PGM254740.html), our little example above is _legacy code_.  
Just because it's not under test.  
That's rude <i class="fa fa-frown-o"></i> but I can't disagree.

## Writing unit test

So, now, let's start over in TDD. Our handler will have a little more logic.

First, the test:
```go
package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandlerReturnsGreetingsWithQueryParameter(t *testing.T) {
	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "http://localhost:8000/a-path?name=lioda", nil) // step 1

	handler(res, req) // step 2

	content, _ := ioutil.ReadAll(res.Body)
	expected := "Hi there, my name is lioda!"
	if string(content) != expected { // step 3
		t.Errorf("Expected %s, got %s", expected, string(content))
	}
}

```

The `TestHandlerReturnsGreetingsWithQueryParameter` function has 3 step:

1. build a faked response to record what happens, and a request corresponding to the handler with a query parameter `name`
1. call the handler
1. assert the response body contains the expected message

Note that method name is also selected to express test intention.

The test, obviously, isn't yet passing:
```bash
--- FAIL: TestHandlerReturnsGreetingsWithQueryParameter (0.00s)
	main_test.go:19: Expected Hi there, my name is lioda!, got hello world
FAIL
exit status 1

```

So, let's write `handler()` method solving this test:

```go
func handler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	fmt.Fprintf(w, "Hi there, my name is %s!", r.FormValue("name"))
}
```

See the test passing:

```bash
PASS
```

## Improving test readability

At first sight, all seems OK, but the test could be cleaner.

- In Unit Test, the whole URL is useless: `http://localhost:8000/a-path?name=lioda`.  
 __The only information is query parameter `?name=lioda`__, nothing else is used:
	- no real HTTP server, we can remove `http://localhost:8000`
	- no router started, we can remove the handler path `a-path`
- assertion style `if () { t.Errorf(...) }` adds some noise. I'll use [testify](https://github.com/stretchr/testify) to improve syntax

Cleaner version:
```go
package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHandlerReturnsGreetingsWithQueryParameter(t *testing.T) {
	res := httptest.NewRecorder()

	handler(res, queryGETWithParameter("?name=lioda"))

	assert.Equal(t, "Hi there, my name is lioda!", readBody(res))
}

func queryGETWithParameter(params string) *http.Request {
	req, _ := http.NewRequest("GET", "URL"+params, nil)
	return req
}
func readBody(res *httptest.ResponseRecorder) string {
	content, _ := ioutil.ReadAll(res.Body)
	return string(content)
}
```

The `urlGET()` and `readBody()` methods are some utilities I use to remove noise from my test code.  
Now, only important information is visible in `TestHandlerReturnsGreetingsWithQueryParameter()`.  
Note that, in tests, there is no need to manage errors. At worst, it fails, and it's exactly what we want.

# A bit more complex server

In first version we were using an optional query parameter to set up user name. In fact, URL should be `/greetings/lioda`.

We will use the [Gorilla Mux web toolkit](http://www.gorillatoolkit.org/pkg/mux) to manage our router.

In TDD, first we adapt the test:
```go
package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestHandlerReturnsGreetingsWithQueryParameter(t *testing.T) {
	res := httptest.NewRecorder()

	handler(res, urlGet("greetings/lioda"))

	assert.Equal(t, "Hi there, my name is lioda!", readBody(res))
}

func urlGet(params string) http.Request {
	req, _ := http.NewRequest("GET", "URL", nil)
	req = mux.SetURLVars(req, map[string]string{"name": "lioda"})
	return req
}
func readBody(res *httptest.ResponseRecorder) string {
	content, _ := ioutil.ReadAll(res.Body)
	return string(content)
}
```

> Note that `URL` is still not used in our unit test. The parameter is artificially set in request by `mux.SetURLVars()` which exists for [test purpose only](http://www.gorillatoolkit.org/pkg/mux#SetURLVars).

Run it, see test fail, then, adapt production code by following Gorilla's documentation:
```go
package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, my name is %s!", mux.Vars(r)["name"])
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/greetings/{name}", handler)
	http.Handle("/", r)
	http.ListenAndServe(":8000", nil)
}


```

The last awkward detail is the use of `mux.SetURLVars()` in test. Gorilla's project explains:

> Alternatively, URL variables can be set by making a route that captures the required variables, starting a server and sending the request to that server.

They are right, but if we start a server, it's no more Unit Test.

# Integration testing

The unit test is done, but we have not yet tested our router. Is handler well configured? Is it listening on good path?

We have to test some part currently in `main()` function, but it's not Unit Testing. When many components are under tests, its called `Integration tests`.

In Integration Tests, we still don't want to start a real server, because we don't want to manage some complexity, like:

* finding a free port
* exposing HTTPS

> These problems will be solved by `System tests` or `End to End tests`. They need deployed environment and a browser-like program. We will not cover this in this article.

here, we want to test the `Router` configuration. It still need mocks for request and response.

So, the test should like:
```go
func TestGreetingsHandlerWithPathParameter(t *testing.T) {
	res := httptest.NewRecorder()

	newRouter().ServeHTTP(res, GET("/greetings/lioda"))

	assert.Equal(t, "Hi there, my name is lioda!", readBody(res))
}

func GET(url string) *http.Request {
	req, _ := http.NewRequest("GET", url, nil)
	return req
}
```

What is this `newRouter()` method? It's our design proposal:
```go
package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, my name is %s!", mux.Vars(r)["name"])
}

func newRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/greetings/{name}", handler)
	return r
}

func main() {
	http.Handle("/", newRouter())
	http.ListenAndServe(":8000", nil)
}

```

The `newRouter()` creates and returns the `Router`. In `main()` we use it to configure server; in test we use it to simulate request.  
Look at test, URL is parsed and parameters are no more injected.

It doesn't replace Unit Tests, it's just another level. More complexity means more difficulty to test every cases, so we still need unit tests.  
With both, we can now be more confident about our code.

## Dependency injection

Another advantage of `newRouter()` method is that it can take some parameters if handlers need dependency injection.

Take an example:
```go
func TestTimeHandler(t *testing.T) {
	res := httptest.NewRecorder()

	newRouter().ServeHTTP(res, GET("/time"))

	assert.Equal(t, "13:37:00", readBody(res))
}
```

We write test covering a not yet implemented `timeHandler()` which returns hour of the day.

We want a fixed clock, because tests use fixed inputs, that's why we expect `13:37:00` and we don't use `time.Now()`. Otherwise, our test would be different at each run... so, in fact, we would test nothing...

To do so `newRouter()` must accept a new parameter: its dependency.

Definitive test is:
```go
func TestTimeHandler(t *testing.T) {
	res := httptest.NewRecorder()

	newRouter(mockClock("13:37:00")).ServeHTTP(res, GET("/time"))

	assert.Equal(t, "13:37:00", readBody(res))
}

func mockClock(fixedValue string) func() time.Time {
	t, _ := time.Parse("15:04:05", fixedValue)
	return func() time.Time {
		return t
	}
}

```

To understand what `mockClock` is, take a look to production code:
```go
type clock func() time.Time

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, my name is %s!", mux.Vars(r)["name"])
}
func newTimeHandler(effectiveClock clock) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, effectiveClock().Format("15:04:05"))
	}
}

func newRouter(effectiveClock clock) *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/greetings/{name}", handler)
	r.HandleFunc("/time", newTimeHandler(effectiveClock))
	return r
}
```


We define a `clock` type function that exposes current time and we mock it in tests.  
`newRouter()` pass it to factory function `newTimeHandler()` which creates the real handler by a  [closure](https://en.wikipedia.org/wiki/Closure_(computer_programming)), keeping reference to `effectiveClock` instance.

In `main()` the clock is real, calling system clock:
```go
func systemNow() time.Time {
	return time.Now()
}

func main() {
	http.Handle("/", newRouter(systemNow))
	http.ListenAndServe(":8000", nil)
}
```

This is a simple dependency injection.

# Conclusion

This short journey through TDD shows how to put even a simple code under tests, with two flavors: unit tests and integration tests.

By some little adjustement, we can keep tests simple. Think about _injection dependency_ and _intention in code_.
