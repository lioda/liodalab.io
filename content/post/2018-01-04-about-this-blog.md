---
title: "About this blog"
date: 2018-01-04
draft: false
image: "images/20180104/cover.jpeg"
description: "I've seen code you people couldn't believe. Bad design on fire ending in NullPointerException. I watched C software run in the shell near the kernel space. I hope all those moments will not be lost in time, like tears of code in rain. Time to blog."
---

# Welcome in my Crafting Room

I started programming a long time ago, when I was nearly seven. It was literally another century. I became professional, but I must admit that the work, and the way I see it, has changed a lot during theses last fifteen years.


I am now a fulltime software architect, TDD evangelist and microservices architect. I spend a lot of time training developers, and it's a hard thing to transmit knowledge. I've discovered that, in our job, each formation is about two things:

* Technical skills, like programming languages, third party libraries, etc.
* Craftsmanship, including TDD, agility, the meaning of design patterns...

## A software developper's job

Craftsmanship is by far the most complicated to teach. It's all about methodologies and mindset; it affects work organization as well as concept like tests and task completion.

It's hard to explain, but it's the most important side of what we do.

However, it's impossible to transform an assembly line worker in a craftsman without good will from the learner. Craftsmanship is not just a bunch of keywords with a syntax to memorize like when you learn another boring C-derived language. It needs to question your way of coding.

And the first lesson I have to enunciate in each session, is exactly that: `your job is not about code`.

## Programming and Coding

Coding is what we do when we transform an algorithm into lines of code, classes and API. It's mainly a routine operation. Nowadays [machines can achieve this without human contribution](https://futurism.com/googles-new-ai-is-better-at-creating-ai-than-the-companys-engineers/). In the future, IA improvements will permit them to do it more and more.

Programming is the engineer task. A complex operation which asks for the use of science, modelisation, creativity, abstraction, language manipulation, and so on.

When we code, we feel comfortable. It's not easy, but we are trained.

When we program we are crafting new ways to solve fresh problems. There is no training, only the engineer methods.

By coding, we create code. By programming we're adding features, which is the real aim of our job.

# Journey towards Craftsmanship

Despite my years of experience, I'm always a learner, because computer programming is a strange field of activity, always in movement. I have to learn so many things, and I will always have to.

This is my passion, and I think this is its beauty. What I know today will be outdated tomorrow, but my current knowledge will be the base of my future understanding.

I permit myself to paraphrase Gandhi:

> Code as if your project was to end tomorrow. Learn as if you were to live forever.


This is this blog's reason. The best way to learn something is to teach it. During my daytime job, I do it a lot, but it's not sufficient. Here I'll write my ideas, organize them, and share my projects.

It's only for me, to progress and improve my skills.

This is my little tears of code in rain.
