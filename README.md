# Lioda's Blog - Tears of Code in Rain

Welcome!

My craft is my code, my code is my craft. I'm a French software architect, and it's my blog where I share some reflexions on my job and my knowledge about coding languages, computers and all the hacking stuff.

## Stack

Using [HUGO](https://gohugo.io) to generate the blog.

## Run the blog in local dev

```bash
docker build -t hugo .
docker run --rm -ti -u 1000 -v $(pwd):/blog -p 1313:1313 hugo serve --bind 0.0.0.0 -D
```

Access at [http://localhost:1313](http://localhost:1313)

# Certificates

Did it following this tuto: https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/
